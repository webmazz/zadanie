var map,
    i = 1,
    markers = [];

$(document).ready(function() {

    initMap();

});

var initMap = function() {
    var mapOptions = {
        center: [52.2297700, 21.0117800],
        zoom: 10
    };

    map = L.map('leaflet-map', mapOptions);

    showLayersOnMap();
    bindEventsToMap();
};

var showLayersOnMap = function() {
    L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
        maxZoom: 18,
        id: 'mapbox.streets',
        accessToken: 'pk.eyJ1Ijoid2VibWF6eiIsImEiOiJjanV3bHQxYnEwZHd4M3ludWR3eWF6bGRnIn0.t5fIVbU6g5m8uLhsl7fKpQ'
    }).addTo(map);
};

var bindEventsToMap = function() {
    map.on('click', function(e) {
        addMarker(e);
    });
};

var addMarker = function(e) {
    var marker = new L.Marker(e.latlng, {
        draggable: 'true'
    }).addTo(map);

    marker.properties = {
        id: i,
        name: 'Marker ' + i
    };

    i++;

    markers.push(marker);

    addMarkerToTable(marker);
    bindEventsToMarker(marker);
};

var bindEventsToMarker = function(marker) {
    marker.bindPopup(marker.properties.name);

    marker.on('dragend', function(e) {
        var markerId = e.target.properties.id,
            position = marker.getLatLng(),
            newLat = position.lat,
            newLng = position.lng;

        updateMarkerInArray(markerId, newLat, newLng);
        updateMarkerLatLngInTable(markerId, newLat, newLng);
    });
};

var addMarkerToTable = function(marker) {
    var tableRowTemplate = '<tr id="marker-data-'+ marker.properties.id +'">' +
                                '<td class="name align-middle">'+ marker.properties.name +'</td>' +
                                '<td class="lat align-middle">'+ marker._latlng.lat +'</td>' +
                                '<td class="lng align-middle">'+ marker._latlng.lng +'</td>' +
                                '<td class="text-center align-middle">' +
                                    '<button type="button" class="btn btn-primary mx-1 open-edit-modal"><i class="fas fa-pencil-alt"></i></button>' +
                                    '<button type="button" class="btn btn-danger mx-1 open-delete-modal"><i class="fas fa-trash"></i></button>' +
                                '</td>' +
                           '</tr>';

    $('#table-markers tbody').append(tableRowTemplate);

    bindEventsToTableRowButtons(marker.properties.id);
};

var updateMarkerInArray = function(markerId, newLat, newLng) {
    var markerFromArray = _.find(markers, function(obj) {
        return obj.properties.id == markerId
    });

    markerFromArray._latlng.lat = newLat;
    markerFromArray._latlng.lng = newLng;
};

var updateMarkerLatLngInTable = function(markerId, newLat, newLng) {
    var tableRow = $('#table-markers tbody').find('#marker-data-' + markerId);

    tableRow.find('.lat').empty().html(newLat);
    tableRow.find('.lng').empty().html(newLng);
};

var bindEventsToTableRowButtons = function(markerId) {
    var buttonEdit = $('#table-markers tbody #marker-data-' + markerId).find('.open-edit-modal'),
        buttonDelete = $('#table-markers tbody #marker-data-' + markerId).find('.open-delete-modal');

    buttonEdit.on('click', function() {
        openModal(markerId, 'edit');
    });

    buttonDelete.on('click', function() {
        openModal(markerId, 'delete');
    });
};

var openModal = function(markerId, modalType) {
    var modalTemplate,
        markerData = _.find(markers, function(obj) {
            return obj.properties.id == markerId
        });

    if (modalType == 'edit') {
        modalTemplate = '<div class="modal fade" id="marker-modal-'+ markerId +'" tabindex="-1" role="dialog" aria-labelledby="editMarkerModal" aria-hidden="true">' +
                            '<div class="modal-dialog" role="document">' +
                                '<div class="modal-content">' +
                                    '<div class="modal-header">' +
                                        '<h5 class="modal-title" id="editMarkerModal">Edycja</h5>' +
                                        '<button type="button" class="close" data-dismiss="modal" aria-label="Zamknij"><span aria-hidden="true">&times;</span></button>' +
                                    '</div>' +
                                    '<div class="modal-body">' +
                                        '<form id="edit-marker-form">' +
                                            '<div class="form-group">' +
                                                '<label for="name">Nazwa</label>' +
                                                '<input type="text" class="form-control" id="name" placeholder="Wpisz nazwę markera" value="'+ markerData.properties.name +'">' +
                                            '</div>' +
                                            '<div class="text-right">' +
                                                '<button type="button" class="btn btn-secondary mr-1" data-dismiss="modal">Anuluj</button>' +
                                                '<button type="submit" class="btn btn-primary ml-1">Zapisz</button>' +
                                            '</div>' +
                                        '</form>' +
                                    '</div>' +
                                '</div>' +
                            '</div>' +
                        '</div>';
    } else if (modalType == 'delete') {
        modalTemplate = '<div class="modal fade" id="marker-modal-'+ markerId +'" tabindex="-1" role="dialog" aria-labelledby="editMarkerModal" aria-hidden="true">' +
                            '<div class="modal-dialog" role="document">' +
                                '<div class="modal-content">' +
                                    '<div class="modal-header">' +
                                        '<h5 class="modal-title" id="editMarkerModal">Edycja</h5>' +
                                        '<button type="button" class="close" data-dismiss="modal" aria-label="Zamknij"><span aria-hidden="true">&times;</span></button>' +
                                    '</div>' +
                                    '<div class="modal-body">' +
                                        '<p>Czy na pewno chcesz usunąć ten marker?</p>' +
                                        '<div class="text-right">' +
                                            '<button type="button" class="btn btn-secondary mr-1" data-dismiss="modal">Anuluj</button>' +
                                            '<button type="button" class="delete-marker btn btn-danger ml-1">Usuń</button>' +
                                        '</div>' +
                                    '</div>' +
                                '</div>' +
                            '</div>' +
                        '</div>';
    }

    $('body').append(modalTemplate).promise().done(function() {
        showModal(markerId);
    });
};

var showModal = function(markerId) {
    $('#marker-modal-' + markerId).modal('show');

    bindEventsToModal(markerId);
};

var bindEventsToModal = function(markerId) {

    $('#marker-modal-' + markerId).on('hidden.bs.modal', function (e) {
        $('#marker-modal-' + markerId).remove();
    });

    $('#marker-modal-' + markerId).find('#edit-marker-form').on('submit', function(e) {
       e.preventDefault();

       var newMarkerName = $('#marker-modal-' + markerId + ' #edit-marker-form').find('input#name').val();
       updateMarkerName(markerId, newMarkerName);
    });

    $('#marker-modal-' + markerId).find('.delete-marker').on('click', function() {
        deleteMarker(markerId);
    });
};

var updateMarkerName = function(markerId, newMarkerName) {
    var marker = _.find(markers, function(obj) {
        return obj.properties.id == markerId
    });

    marker.properties.name = newMarkerName;

    marker.bindPopup(marker.properties.name);

    var tableRow = $('#table-markers tbody').find('#marker-data-' + markerId);
    tableRow.find('.name').empty().html(newMarkerName);

    $('#marker-modal-' + markerId).modal('hide');
};

var deleteMarker = function(markerId) {
    var marker = _.find(markers, function(obj) {
        return obj.properties.id == markerId
    });

    map.removeLayer(marker);
    _.remove(markers, function(obj) {
        return obj.properties.id == markerId
    });

    var tableRow = $('#table-markers tbody').find('#marker-data-' + markerId);
    tableRow.remove();

    $('#marker-modal-' + markerId).modal('hide');
};